<?php

namespace Auth\Event;

use Zend\Mvc\MvcEvent as MvcEvent;
use Zend\Mvc\Controller\AbstractActionController;
use Auth\Controller\Plugin\UserAuthentication as AuthPlugin;
//use Auth\Acl\Acl as AclClass;

class Authentication
{
    protected $_aclClass = null;
    
    public function preDispatch(MvcEvent $event)
    {
	$userAuth = $event->getTarget()->userAuthentication();
//        $acl = $this->getAclClass();

        if ($userAuth->hasIdentity()) {
            $user = $userAuth->getIdentity();
	    //@todo - Get role from user!
            $role = 'member';
        } else {
            $role = 'guest';
//          $role = AclClass::DEFAULT_ROLE;
	}


        $routeMatch = $event->getRouteMatch();
        $controller = $routeMatch->getParam('controller');
        $action     = $routeMatch->getParam('action');
/*
        if (!$acl->hasResource($controller)) {
            throw new \Exception('Resource ' . $controller . ' not defined');
        }
*/

        if ($role == 'guest' && $action != 'login' && $action != 'authenticate') {
//        if (!$acl->isAllowed($role, $controller, $action)) {
            $url = $event->getRouter()->assemble(array(), array('name' => 'login'));
            $response = $event->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $url);
            $response->setStatusCode(302);
            $response->sendHeaders();
//	    exit;
	    return $response;
        }
	$application = $event->getApplication();
	$sm = $application->getServiceManager();
//	$view = $sm->get('ViewManager')->getView();
	$navigation = $sm->get('Navigation');
	$pages = new \Zend\Navigation\Page\Mvc(
		array(
                    'label' => 'Logout',
                    'action' => 'logout',
                    'route' => 'login/process',
		));
	$routeMatch = $application->getMvcEvent()->getRouteMatch();
	$router = $application->getMvcEvent()->getRouter();
	$pages->setRouteMatch($routeMatch);
	$pages->setDefaultRouter($router);
	$navigation->addPage($pages);
    }

/*
    public function setAclClass(AclClass $aclClass)
    {
        $this->_aclClass = $aclClass;

        return $this;
    }

    public function getAclClass()
    {
        if ($this->_aclClass === null) {
            $this->_aclClass = new AclClass(array());
        }

        return $this->_aclClass;
    }
*/
}
