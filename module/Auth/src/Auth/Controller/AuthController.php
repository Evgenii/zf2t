<?php

namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;

use Auth\Model\User;

class AuthController extends AbstractActionController
{
	protected $form;
	
	public function getForm()
	{
		if (! $this->form) {
			$user = new User();
			$builder = new AnnotationBuilder();
			$this->form = $builder->createForm($user);
		}
		
		return $this->form;
	}
	
	public function loginAction()
	{
		//if already login, redirect to success page
		if ($this->identity()) {
			return $this->redirect()->toRoute('success');
		}
                
		$form = $this->getForm();
		$this->layout('layout/layout-guest');
		
		return array(
		    'form'      => $form,
		    'messages'  => $this->flashmessenger()->getMessages()
		);
	}
	
	public function authenticateAction()
	{
		$form = $this->getForm();
		$redirect = 'login';
		$request = $this->getRequest();
		
		if ($request->isPost()){
			$form->setData($request->getPost());
			if ($form->isValid()){
				//check authentication...
				$userAuth = $this->userAuthentication();
				$userAuth->getAuthAdapter()
                                       ->setIdentity($request->getPost('username'))
                                       ->setCredential($request->getPost('password'));
				
				$result = $userAuth->getAuthService()->authenticate();
				foreach($result->getMessages() as $message)
				{
					//save message temporary into flashmessenger
					$this->flashmessenger()->addMessage($message);
				}
				
				if ($result->isValid()) {
					$redirect = 'success';
					//check if it has rememberMe :
					if ($request->getPost('rememberme') == 1 ) {
						$userAuth->getSessionStorage()->setRememberMe(1);
						//set storage again
						$userAuth->setSessionStorage($userAuth->getSessionStorage());
					}
					$userAuth->setSessionStorage($userAuth->getSessionStorage());
					$userAuth->getAuthService()->getStorage()->write($request->getPost('username'));
				}
			}
		}
		
		return $this->redirect()->toRoute($redirect);
	}
	
	public function logoutAction()
	{
		if ($this->identity()) {
			$uAuth = $this->userAuthentication();
			$uAuth->getSessionStorage()->forgetMe();
			$uAuth->getAuthService()->clearIdentity();
			$this->flashmessenger()->addMessage("You've been logged out");
		}
		
		return $this->redirect()->toRoute('login');
	}
}
