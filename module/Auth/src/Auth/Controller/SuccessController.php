<?php

namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SuccessController extends AbstractActionController
{
	public function indexAction()
	{
		if (!$this->identity()) {
			return $this->redirect()->toRoute('login');
		}
		return $this->redirect()->toRoute('home');
		
		return new ViewModel();
	}
}
