<?php

namespace Auth\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;

class UserAuthentication extends AbstractPlugin
{
    protected $_authService;
    protected $_authStorage;

    public function hasIdentity()
    {
        return $this->getAuthService()->hasIdentity();
    }

    public function getIdentity()
    {
        return $this->getAuthService()->getIdentity();
    }

    public function setAuthAdapter(AuthAdapter $authAdapter)
    {
        $this->getAuthService()->setAdapter($authAdapter);

        return $this;
    }

    public function getAuthAdapter()
    {
	return $this->getAuthService()->getAdapter();
    }
    
    public function setSessionStorage($authStorage)
    {
	$this->getAuthService()->setStorage($authStorage);
        $this->_authStorage = $authStorage;

        return $this;
    }
    
    public function getSessionStorage()
    {
        if (!$this->_authStorage) {
            $this->_authStorage = $this->getController()->getServiceLocator()
                                  ->get('Auth\Model\AuthStorage');
        }
        
        return $this->_authStorage;
    }

    public function setAuthService(AuthenticationService $authService)
    {
        $this->_authService = $authService;

        return $this;
    }

    public function getAuthService()
    {
        if (!$this->_authService) {
            $this->setAuthService(
		    $this->getController()->getServiceLocator()->get('AuthService')
	    );
        }

        return $this->_authService;
    }

}
