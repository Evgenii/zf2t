<?php

namespace Auth\Service;

use Zend\ServiceManager\DelegatorFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

class AuthServiceDelegatorFactory implements DelegatorFactoryInterface
{
	public function createDelegatorWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName, $callback)
	{
		$authService = call_user_func($callback);
		$dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
                $dbTableAuthAdapter  = new DbTableAuthAdapter($dbAdapter, 'user', 'username', 'password', 'MD5(?)');
		$authService->setAdapter($dbTableAuthAdapter);
		$authService->setStorage($serviceLocator->get('Auth\Model\AuthStorage'));
		return $authService;
	}
}
