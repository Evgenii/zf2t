<?php

namespace Auth;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;

use Zend\EventManager\EventInterface as Event;

//use Zend\Authentication\AuthenticationService;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
	public function onBootstrap(Event $e)
	{
		$sharedEvents = $e->getApplication()->getEventManager()->getSharedManager();
//@todo - Go directly to User\Event\Authentication
		$sharedEvents->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array($this, 'mvcPreDispatch'), 100);
	}
	
    public function getServiceConfig()
    {
        return array(
	    'delegators' => array(
		'Zend\Authentication\AuthenticationService' => array(
			'Auth\Service\AuthServiceDelegatorFactory',
		),
	    ),
            'factories' => array(
		'Auth\Model\AuthStorage' => function($sm){
		    return new \Auth\Model\AuthStorage('zf2t');
		},
		
/*
		'AuthService' => function($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter  = new DbTableAuthAdapter($dbAdapter, 'user','username','password', 'MD5(?)');
		    $authService = new AuthenticationService();
		    $authService->setAdapter($dbTableAuthAdapter);
		    $authService->setStorage($sm->get('Auth\Model\AuthStorage'));
		    return $authService;
		    return $sm->get('Zend\Authentication\AuthenticationService');
 		},
*/		     
            ),
        );
    }
    
	public function mvcPreDispatch(Event $event)
	{
		$locator = $event->getTarget()->getServiceLocator();
		$auth = $locator->get('Auth\Event\Authentication');
		return $auth->preDispatch($event);
//		return false;
	}
}
