<?php

namespace Album\ControllerFactory;
use \Zend\ServiceManager\FactoryInterface;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Album\Controller\AlbumController;

class AlbumControllerFact implements FactoryInterface
{

	public function createService(ServiceLocatorInterface $serviceLocator)
	{
        /* @var $serviceLocator \Zend\Mvc\Controller\ControllerManager */
		$sm   = $serviceLocator->getServiceLocator();
		$logger = $sm->get('Album:Logger');
		$path = $sm->get('LoggerPath');
		$dbconfig = array(
			// Sqlite Configuration
			'driver' => 'Pdo',
			'dsn' => 'sqlite:' . $path . 'album_detailed.db',
//			'dsn' => 'sqlite:' . __DIR__ . "/$path" . 'album_detailed.db',
		);
		$dbconfig2 = array(
			// Sqlite Configuration
			'driver' => 'Pdo_Sqlite',
			'database' => $path . 'album_detailed2.db',
		);
		$db = new \Zend\Db\Adapter\Adapter($dbconfig2);
		$mapping = array(
			'timestamp' => 'date',
			'priority'  => 'type',
			'message'   => 'event'
		);
//		$writer = new \Zend\Log\Writer\Db($db, 't_controllers', $mapping);
//		$logger->addWriter($writer);
		
		$controller = new AlbumController($logger);
		$sharedEvents = $controller->getEventManager()->getSharedManager();
		$sharedEvents->attach(get_class($controller), array('indexAction', 'addAction', 'editAction', 'deleteAction'),
			function ($e) use ($controller) {
				$event = $e->getName();
				$target = get_class($e->getTarget());
				$params = $e->getParams();
				$message = sprintf(
						'[%s] [%s] Cached datas: %s',
						$event,
						$target,
						json_encode($params)
					);
				$controller->getEventManager()->trigger($event . '.log', $e->getTarget(), array('message' => $message, 'debug' => true));
			});
		return $controller;
	}
}
