<?php

// module/Album/src/Album/Controller/AlbumController.php:
namespace Album\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Album\Model\Album;          // <-- Add this import
use Album\Form\AlbumForm;       // <-- Add this import
use Album\Form\AuthForm;
use Album\Model\Auth;
use Config\ConfigAwareInterface;

class AlbumController extends AbstractActionController implements ConfigAwareInterface
{
	protected $albumTable;
	protected $logger;
	public $config;
	
	public function __construct(\Zend\Log\Logger $logger)
	{
		$this->getEventManager()->trigger('log', $this, array(__FUNCTION__ . ' is starting...'));
		$this->logger = $logger;
	}
	
	public function setConfig($config)
	{
		$this->config = $config;
	}
	
	public function indexAction()
	{
		return new ViewModel(array(
				'albums' => $this->getAlbumTable()->fetchAll(),
			));
	}
	
    public function loginAction()
    {
        $form = new AuthForm();
        $form->get('submit')->setValue('Enter');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $auth = new Auth();
            $form->setInputFilter($auth->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                return $this->redirect()->toRoute('album');
            }
        }
        return array('form' => $form);
    }
    
    public function addAction()
    {
        $form = new AlbumForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $album = new Album();
            $form->setInputFilter($album->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $album->exchangeArray($form->getData());
		$this->getEventManager()->trigger(__FUNCTION__, $this, $form->getData());
                $this->getAlbumTable()->saveAlbum($album);

                // Redirect to list of albums
                return $this->redirect()->toRoute('album');
            }
        }
        return array('form' => $form);
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('album', array(
                'action' => 'add'
            ));
        }
        $album = $this->getAlbumTable()->getAlbum($id);

        $form  = new AlbumForm();
        $form->bind($album);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($album->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
		$this->getEventManager()->trigger(__FUNCTION__, $this, $form->getData());
                $this->getAlbumTable()->saveAlbum($form->getData());

                // Redirect to list of albums
                return $this->redirect()->toRoute('album');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('album');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
		$this->getEventManager()->trigger(__FUNCTION__, $this, compact('id'));
		$this->getAlbumTable()->deleteAlbum($id);
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('album');
        }

        return array(
            'id'    => $id,
            'album' => $this->getAlbumTable()->getAlbum($id)
        );
    }

    public function getAlbumTable()
    {
        if (!$this->albumTable) {
            $sm = $this->getServiceLocator();
            $this->albumTable = $sm->get('Album\Model\AlbumTable');
        }
        return $this->albumTable;
    }
}