<?php

namespace Logger\Service;

use Zend\ServiceManager\DelegatorFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoggerDelegatorFactory implements DelegatorFactoryInterface
{
	public function createDelegatorWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName, $callback)
	{
		$defaultLogger   = call_user_func($callback);
		$path = $serviceLocator->get('LoggerPath');
		$writer = new \Zend\Log\Writer\Stream($path . 'logger.log');
		$defaultLogger->addWriter($writer);
		return $defaultLogger;
	}
}
